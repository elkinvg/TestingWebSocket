/*----- PROTECTED REGION ID(TestingWebSocket.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        TestingWebSocket.cpp
//
// description : C++ source for the TestingWebSocket class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               TestingWebSocket are implemented in this file.
//
// project :     Tango class for testing WebSocketDS
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <TestingWebSocket.h>
#include <TestingWebSocketClass.h>

/*----- PROTECTED REGION END -----*/	//	TestingWebSocket.cpp

/**
 *  TestingWebSocket class description:
 *    Tango class for testing WebSocketDS.
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name   |  Method name
//================================================================
//  State          |  Inherited (no method)
//  Status         |  Inherited (no method)
//  ComDevShort    |  com_dev_short
//  ComDevFloat    |  com_dev_float
//  UpdateData     |  update_data
//  GetDouble      |  get_double
//  DoubleDouble   |  double_double
//  GetMassDouble  |  get_mass_double
//  MassDouble     |  mass_double
//  getStringArr   |  get_string_arr
//  getStrr        |  get_strr
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  AttrDevShort         |  Tango::DevShort	Scalar
//  AttrDevDouble        |  Tango::DevDouble	Scalar
//  AttrDevFloat         |  Tango::DevFloat	Scalar
//  AttrDevDouble2       |  Tango::DevDouble	Scalar
//  AttrDevDouble3       |  Tango::DevDouble	Scalar
//  AttrDevDouble4       |  Tango::DevDouble	Scalar
//  bool_attr            |  Tango::DevBoolean	Scalar
//  AttrDevUShortSpectr  |  Tango::DevUShort	Spectrum  ( max = 100)
//  AttrDevFloatSpectr   |  Tango::DevFloat	Spectrum  ( max = 3)
//  bool_spectr          |  Tango::DevBoolean	Spectrum  ( max = 20)
//  AttrDevLong64Image   |  Tango::DevLong64	Image  ( max = 100 x 100)
//  AttrDevDoubleImage   |  Tango::DevDouble	Image  ( max = 2 x 2)
//================================================================

namespace TestingWebSocket_ns
{
/*----- PROTECTED REGION ID(TestingWebSocket::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::TestingWebSocket()
 *	Description : Constructors for a Tango device
 *                implementing the classTestingWebSocket
 */
//--------------------------------------------------------
TestingWebSocket::TestingWebSocket(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(TestingWebSocket::constructor_1) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::constructor_1
}
//--------------------------------------------------------
TestingWebSocket::TestingWebSocket(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(TestingWebSocket::constructor_2) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::constructor_2
}
//--------------------------------------------------------
TestingWebSocket::TestingWebSocket(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(TestingWebSocket::constructor_3) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void TestingWebSocket::delete_device()
{
	DEBUG_STREAM << "TestingWebSocket::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::delete_device) ENABLED START -----*/

	//	Delete device allocated objects

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::delete_device
	delete[] attr_AttrDevShort_read;
	delete[] attr_AttrDevDouble_read;
	delete[] attr_AttrDevFloat_read;
	delete[] attr_AttrDevDouble2_read;
	delete[] attr_AttrDevDouble3_read;
	delete[] attr_AttrDevDouble4_read;
	delete[] attr_bool_attr_read;
	delete[] attr_AttrDevUShortSpectr_read;
	delete[] attr_AttrDevFloatSpectr_read;
	delete[] attr_bool_spectr_read;
	delete[] attr_AttrDevLong64Image_read;
	delete[] attr_AttrDevDoubleImage_read;
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void TestingWebSocket::init_device()
{
	DEBUG_STREAM << "TestingWebSocket::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::init_device_before) ENABLED START -----*/

	//	Initialization before get_device_property() call

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	
	attr_AttrDevShort_read = new Tango::DevShort[1];
	attr_AttrDevDouble_read = new Tango::DevDouble[1];
	attr_AttrDevFloat_read = new Tango::DevFloat[1];
	attr_AttrDevDouble2_read = new Tango::DevDouble[1];
	attr_AttrDevDouble3_read = new Tango::DevDouble[1];
	attr_AttrDevDouble4_read = new Tango::DevDouble[1];
	attr_bool_attr_read = new Tango::DevBoolean[1];
	attr_AttrDevUShortSpectr_read = new Tango::DevUShort[100];
	attr_AttrDevFloatSpectr_read = new Tango::DevFloat[3];
	attr_bool_spectr_read = new Tango::DevBoolean[20];
	attr_AttrDevLong64Image_read = new Tango::DevLong64[100*100];
	attr_AttrDevDoubleImage_read = new Tango::DevDouble[2*2];
	/*----- PROTECTED REGION ID(TestingWebSocket::init_device) ENABLED START -----*/
    attr_AttrDevShort_read[0] = 0;
    attr_AttrDevDouble_read[0] = 0;
    attr_AttrDevFloat_read[0] = 0;

    attr_AttrDevDouble2_read[0] = 0;
    attr_AttrDevDouble3_read[0] = 0;
    attr_AttrDevDouble4_read[0] = 0;

    attr_bool_attr_read[0] = true;

    for (int i = 0; i < 3; i++) {
        attr_AttrDevFloatSpectr_read[i] = 0;
    }

    for (int i = 0; i < 2*2; i++) {
        attr_AttrDevDoubleImage_read[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
        attr_AttrDevUShortSpectr_read[i] = 0;
    }
    for (int i = 0; i < 100 * 100; i++) {
        attr_AttrDevLong64Image_read[i] = 0;
    }

    for (int i = 0; i < 20; i++) {
        attr_bool_spectr_read[i] = true;
    }
	//	Initialize device

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::init_device
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void TestingWebSocket::get_device_property()
{
	/*----- PROTECTED REGION ID(TestingWebSocket::get_device_property_before) ENABLED START -----*/

	//	Initialize property data members

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("SleepTime"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on TestingWebSocketClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		TestingWebSocketClass	*ds_class =
			(static_cast<TestingWebSocketClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize SleepTime from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  sleepTime;
		else {
			//	Try to initialize SleepTime from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  sleepTime;
		}
		//	And try to extract SleepTime value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  sleepTime;

	}

	/*----- PROTECTED REGION ID(TestingWebSocket::get_device_property_after) ENABLED START -----*/

	//	Check device property data members init

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void TestingWebSocket::always_executed_hook()
{
	DEBUG_STREAM << "TestingWebSocket::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::always_executed_hook) ENABLED START -----*/

	//	code always executed before all requests

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void TestingWebSocket::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "TestingWebSocket::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_attr_hardware) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute AttrDevShort related method
 *	Description: 
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevShort(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevShort(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevShort) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevShort_read);

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevShort
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevDouble related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevDouble(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevDouble(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevDouble) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevDouble_read);

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevDouble
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevFloat related method
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevFloat(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevFloat(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevFloat) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevFloat_read);

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevFloat
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevDouble2 related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevDouble2(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevDouble2(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevDouble2) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevDouble2_read);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevDouble2
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevDouble3 related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevDouble3(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevDouble3(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevDouble3) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevDouble3_read);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevDouble3
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevDouble4 related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevDouble4(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevDouble4(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevDouble4) ENABLED START -----*/
	//	Set the attribute value
    attr.set_value(attr_AttrDevDouble4_read);
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevDouble4
}
//--------------------------------------------------------
/**
 *	Read attribute bool_attr related method
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestingWebSocket::read_bool_attr(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_bool_attr(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_bool_attr) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_bool_attr_read);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_bool_attr
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevUShortSpectr related method
 *	Description: 
 *
 *	Data type:	Tango::DevUShort
 *	Attr type:	Spectrum max = 100
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevUShortSpectr(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevUShortSpectr(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevUShortSpectr) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevUShortSpectr_read, 100);

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevUShortSpectr
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevFloatSpectr related method
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Spectrum max = 3
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevFloatSpectr(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevFloatSpectr(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevFloatSpectr) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevFloatSpectr_read, 3);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevFloatSpectr
}
//--------------------------------------------------------
/**
 *	Read attribute bool_spectr related method
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Spectrum max = 20
 */
//--------------------------------------------------------
void TestingWebSocket::read_bool_spectr(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_bool_spectr(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_bool_spectr) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_bool_spectr_read, 20);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_bool_spectr
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevLong64Image related method
 *	Description: 
 *
 *	Data type:	Tango::DevLong64
 *	Attr type:	Image max = 100 x 100
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevLong64Image(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevLong64Image(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevLong64Image) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevLong64Image_read, 100, 100);

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevLong64Image
}
//--------------------------------------------------------
/**
 *	Read attribute AttrDevDoubleImage related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Image max = 2 x 2
 */
//--------------------------------------------------------
void TestingWebSocket::read_AttrDevDoubleImage(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestingWebSocket::read_AttrDevDoubleImage(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_AttrDevDoubleImage) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_AttrDevDoubleImage_read, 2, 2);
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_AttrDevDoubleImage
}

//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void TestingWebSocket::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(TestingWebSocket::add_dynamic_attributes) ENABLED START -----*/

	//	Add your own code to create and add dynamic attributes if any

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Read pipe PipeAttrs related method
 *	Description: 
 */
//--------------------------------------------------------
void TestingWebSocket::read_PipeAttrs(Tango::Pipe &pipe)
{
	DEBUG_STREAM << "TestingWebSocket::read_PipeAttrs(Tango::Pipe &pipe) entering... " << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::read_PipeAttrs) ENABLED START -----*/
	
    pipe.set_root_blob_name("dataFromTestWs");
    vector<string> names{
        "bool_attr",
        "AttrDevFloatSpectr",
        "bool_spectr"
    };
    pipe.set_data_elt_names(names);
    vector<float> forPipeFloat;
    float *fl = new float[3];
    for (int i = 0; i < 3; i++){
        //cout << "VAL: " << attr_AttrDevFloatSpectr_read[i] << endl;
        forPipeFloat.push_back(attr_AttrDevFloatSpectr_read[i]);
        fl[i] = attr_AttrDevFloatSpectr_read[i];
    }
    Tango::DevVarFloatArray *dfl = create_DevVarFloatArray(fl, 3);

    vector<bool> forPipeBoolVec;
    for (int i = 0; i < 20; i++) {
        forPipeBoolVec.push_back(attr_bool_spectr_read[i]);
    }

    try {
        pipe << attr_bool_attr_read[0]
            << dfl
            << forPipeBoolVec;
    }
    catch (Tango::WrongData &e) {
        ERROR_STREAM << " wrong data in pipe pswData " << endl;
    }
    catch (Tango::DevFailed &e) {
        ERROR_STREAM << e.errors[0].desc << endl;
    }
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::read_PipeAttrs
}
//--------------------------------------------------------
/**
 *	Command ComDevShort related method
 *	Description: 
 *
 *	@param argin 
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevShort TestingWebSocket::com_dev_short(Tango::DevShort argin)
{
	Tango::DevShort argout;
	DEBUG_STREAM << "TestingWebSocket::ComDevShort()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::com_dev_short) ENABLED START -----*/

	//	Add your own code
    argout = argin+100;
    std::uniform_int_distribution<short> distribution(0, 10000);
    argout = distribution(generator);
    sleepCom();
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::com_dev_short
	return argout;
}
//--------------------------------------------------------
/**
 *	Command ComDevFloat related method
 *	Description: 
 *
 *	@param argin 
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevFloat TestingWebSocket::com_dev_float(Tango::DevFloat argin)
{
	Tango::DevFloat argout;
	DEBUG_STREAM << "TestingWebSocket::ComDevFloat()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::com_dev_float) ENABLED START -----*/

	//	Add your own code
    argout = argin*2;
    std::uniform_real_distribution<float> distDouble(0., 5000.0);
    argout = distDouble(generator);
    sleepCom();
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::com_dev_float
	return argout;
}
//--------------------------------------------------------
/**
 *	Command UpdateData related method
 *	Description: 
 *
 */
//--------------------------------------------------------
void TestingWebSocket::update_data()
{
	DEBUG_STREAM << "TestingWebSocket::UpdateData()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::update_data) ENABLED START -----*/
    std::uniform_int_distribution<short> distribution(0, 10000);
    std::uniform_real_distribution<double> distDouble(0., 100.0);
    std::uniform_int_distribution<unsigned short> distrUShortSpectr(0, 10000);
    std::uniform_int_distribution<long> distrLongImage(10000,1000000);

    attr_AttrDevShort_read[0] = distribution(generator);
    //attr_AttrDevDouble_read[0] = distDouble(generator);

    attr_AttrDevDouble_read[0] = 1234567.67890123456789;//distDouble(generator);
    attr_AttrDevFloat_read[0] = 1234567.67890123456789;

    attr_AttrDevDouble2_read[0] = 1476379200;
    attr_AttrDevDouble3_read[0] = 1476379200;
    attr_AttrDevDouble4_read[0] = 1476379200;

    for (int i = 0; i < 100; i++) {
        attr_AttrDevUShortSpectr_read[i] = distrUShortSpectr(generator);
    }

    for (int i = 0; i < 100*100; i++) {
      if (i==0 || i==1 )
        attr_AttrDevLong64Image_read[i] = 8223372036854775807;
      else
        attr_AttrDevLong64Image_read[i] = distrLongImage(generator);
    }

    for (int i = 0; i < 3; i++) {
        attr_AttrDevFloatSpectr_read[i] = 1234567.67890123456789;
    }

    for (int i = 0; i < 2 * 2; i++) {
        attr_AttrDevDoubleImage_read[i] = 1234567.67890123456789;
    }
	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::update_data
}
//--------------------------------------------------------
/**
 *	Command GetDouble related method
 *	Description: 
 *
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevDouble TestingWebSocket::get_double()
{
	Tango::DevDouble argout;
	DEBUG_STREAM << "TestingWebSocket::GetDouble()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::get_double) ENABLED START -----*/
	
	//	Add your own code
    argout = 1234567.8901234567;
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_double
	return argout;
}
//--------------------------------------------------------
/**
 *	Command DoubleDouble related method
 *	Description: 
 *
 *	@param argin 
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevDouble TestingWebSocket::double_double(Tango::DevDouble argin)
{
	Tango::DevDouble argout;
	DEBUG_STREAM << "TestingWebSocket::DoubleDouble()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::double_double) ENABLED START -----*/
	
	//	Add your own code
    argout = argin;
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::double_double
	return argout;
}
//--------------------------------------------------------
/**
 *	Command GetMassDouble related method
 *	Description: 
 *
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevVarDoubleArray *TestingWebSocket::get_mass_double()
{
	Tango::DevVarDoubleArray *argout;
	DEBUG_STREAM << "TestingWebSocket::GetMassDouble()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::get_mass_double) ENABLED START -----*/
	
	//	Add your own code
    argout = new Tango::DevVarDoubleArray();
    argout->length(3);
    (*argout)[0] = 1234567.8901234567;
    (*argout)[1] = 123.45678901234567;
    (*argout)[2] = 1234567890123.4567;
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_mass_double
	return argout;
}
//--------------------------------------------------------
/**
 *	Command MassDouble related method
 *	Description: 
 *
 *	@param argin 
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevVarDoubleArray *TestingWebSocket::mass_double(const Tango::DevVarDoubleArray *argin)
{
	Tango::DevVarDoubleArray *argout;
	DEBUG_STREAM << "TestingWebSocket::MassDouble()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::mass_double) ENABLED START -----*/
	
	//	Add your own code
    argout = new Tango::DevVarDoubleArray();
    argout->length(argin->length());

    for (int i = 0; i < argout->length(); i++) {
        (*argout)[i] = (*argin)[i];
    }
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::mass_double
	return argout;
}
//--------------------------------------------------------
/**
 *	Command getStringArr related method
 *	Description: 
 *
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevVarStringArray *TestingWebSocket::get_string_arr()
{
	Tango::DevVarStringArray *argout;
	DEBUG_STREAM << "TestingWebSocket::getStringArr()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::get_string_arr) ENABLED START -----*/
	
    argout = new Tango::DevVarStringArray();
    argout->length(2);
    string o1 = "out string test1";
    string o2 = "out string test1";
    (*argout)[0] = CORBA::string_dup(o1.c_str());
    (*argout)[1] = CORBA::string_dup(o2.c_str());
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_string_arr
	return argout;
}
//--------------------------------------------------------
/**
 *	Command getStrr related method
 *	Description: 
 *
 *	@returns 
 */
//--------------------------------------------------------
Tango::DevString TestingWebSocket::get_strr()
{
	Tango::DevString argout;
	DEBUG_STREAM << "TestingWebSocket::getStrr()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(TestingWebSocket::get_strr) ENABLED START -----*/
	
    string ttt = "out string test";
    argout = CORBA::string_dup(ttt.c_str());
	
	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::get_strr
	return argout;
}
//--------------------------------------------------------
/**
 *	Method      : TestingWebSocket::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void TestingWebSocket::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(TestingWebSocket::add_dynamic_commands) ENABLED START -----*/

	//	Add your own code to create and add dynamic commands if any

	/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::add_dynamic_commands
}

/*----- PROTECTED REGION ID(TestingWebSocket::namespace_ending) ENABLED START -----*/
void TestingWebSocket::sleepCom() {
    int tmp = sleepTime * 1000;
#ifdef __unix__
    if (tmp>100) usleep(tmp);
#else
    if (tmp>100) Sleep(tmp); // for serialport
#endif
}

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	TestingWebSocket::namespace_ending
} //	namespace
