﻿$(document).ready(function () {
//var protocol = location.protocol == 'https:'? 'wss':'ws';
var protocol = 'ws';
//var wsAddr = protocol + "://" + location.host + "/wsj";
var wsAddr = protocol + "://" + location.host + ":7777?login=tango1&password=tango1";
var socket = new WebSocket(wsAddr);

var id = 0;

$("#test_butt").click(function () {
  argin = {};
  //argin.argin = [23,556,8886,333];
  argin.argin = 23.25;
  argin.type_req = "command";
  argin.command_name =  "ComDevFloat";
  //argin.command_name = "DevBoolean";
  //argin.argin = true;
  //argin.command = "ComDevFloat";
  argin.id = id;
  var sender = JSON.stringify(argin);
  socket.send(sender);
  console.log("but clicked");
  console.log("id= " + id);
    id++;
});

$("#massive_butt").click(function () {
  argin = {};
  argin.command = "ComDevShort";
  argin.id = id;
  argin.argin = 125;
  var sender = JSON.stringify(argin);
  socket.send(sender);
  console.log("but clicked");
  console.log("id= " + id);
  id++;
});

$("#pipe_but").click(function () {
  argin = {};
  argin.type_req = "read_pipe_gr";
  //argin.read_pipe_gr = "string_long_short_ro";
  argin.pipe_name = "PipeAttrs",
  argin.id = id;
  var sender = JSON.stringify(argin);
  socket.send(sender);
  id++;
});

$("#close_butt").click(function () {
  $("#test").append('Соединение закрыто');
  socket.close();
});


socket.onmessage = function(event) {
  //alert("Получены данные " + event.data);
  //alert("Получены данные " + event.data);
  try {
    var fromJson = $.parseJSON(event.data);
    if (fromJson.type_req !== undefined &&  ( fromJson.type_req == "attribute" || fromJson.type_req == "group_attribute") ) {
      $("#test").html("Получены данные " + event.data + "<br><br>");
    }
    else {
      $("#outout").html("Ответ: " + event.data + "<br><br>");
    //if (fromJson.command !== undefined || fromJson.error  !== undefined) {
    //$("#out_from_server").html("Ответ: " + event.data + "<br><br>");
    //$("#outout").append("ID: " + fromJson.id + " | ");
    }
  }
  catch(e) {
    $("#outout").html("JSON PARSED ERROR Ответ: " + event.data + "<br><br>");
  }

  //console.log("Получены данные " + event.data);
};

socket.onerror = function(error) {
  $("#test").append("Ошибка " + error.message);
  socket.close();
};

socket.onclose = function(event) {
  if (event.wasClean) {
    $("#test").append('Соединение закрыто чисто');
    socket.close();
  } else {
    $("#test").append('Обрыв соединения'); // например, "убит" процесс сервера
    socket.close();
  }
  $("#test").append('Код: ' + event.code + ' причина: ' + event.reason);
  socket.close();
};
});
